class ApplicationMailer < ActionMailer::Base
  default from: 'syseydi@gmail.com'
  layout 'mailer'
end
