class ClientMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.client_mailer.account_activation.subject
  #
  def account_activation(client)
    @greeting = "Hi"
    @client = client
    mail to: client.email, subject: "Seydisign Account Activation"
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.client_mailer.passwordreset.subject
  #
  def password_reset(client)
    @client = client
    mail to: client.email, subject: "Password reset"
  end

#Email clients when they receive a new message
  def notification(email)
    @client = Client.where(email: email).first
    @email = email
    @path = login_url
    mail to: @email, subject: "Seydisign Notification"
  end 
  
end
