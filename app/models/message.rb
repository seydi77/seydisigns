class Message < ApplicationRecord
  belongs_to :client
  default_scope -> { order(created_at: :asc) }
  validates :client_id, presence: true
  validates :content, presence: true, length: { maximum: 1400 }
  validates :recipient, presence: true
end

