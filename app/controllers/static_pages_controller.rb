class StaticPagesController < ApplicationController
  def home
  	@message = current_client.messages.build if logged_in?
    if logged_in?
     @feed_items = current_client.feed.order(@message.created_at) 
    end 
end

def help
end

def about
end

def contact
end

end
