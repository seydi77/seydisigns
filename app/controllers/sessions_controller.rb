class SessionsController < ApplicationController

  def new
    redirect_to root_url unless !logged_in?
  end

  def create
    client = Client.find_by(email: params[:session][:email].downcase)
    if client && client.authenticate(params[:session][:password])
       if client.activated?
        log_in client
        params[:session][:remember_me] == '1' ? remember(client) : forget(client)
        redirect_to root_url
      else
        message  = "Account not activated. "
        message += "Check your email for the activation link."
        flash[:warning] = message
        redirect_to root_url
      end
    else
      flash.now[:danger] = 'Invalid email/password combination'
      render 'new'
    end
  end

  def destroy
    log_out if logged_in?
    redirect_to root_url
  end
end