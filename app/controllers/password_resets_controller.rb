class PasswordResetsController < ApplicationController
  before_action :get_client,   only: [:edit, :update]
  before_action :valid_client, only: [:edit, :update]
  before_action :check_expiration, only: [:edit, :update]    # Case (1)

  def new
  end

  def create
    @client = Client.find_by(email: params[:password_reset][:email].downcase)
    if @client
      @client.create_reset_digest
      @client.send_password_reset_email
      flash[:info] = "Email sent with password reset instructions (this may take a few minutes)."
      redirect_to root_url
    else
      flash.now[:danger] = "Email address not found. Please signup."
      render 'new'
    end
  end

  def edit
  end

  def update
    if params[:client][:password].empty?                  # Case (3)
      @client.errors.add(:password, "can't be empty")
      render 'edit'
    elsif @client.update_attributes(client_params)          # Case (4)
      log_in @client
      @client.update_attribute(:reset_digest, nil)
      flash[:success] = "Password has been reset."
      redirect_to @client
    else
      render 'edit'                                     # Case (2)
    end
  end

  private

  def client_params
      params.require(:client).permit(:password, :password_confirmation)
    end

  def get_client
    @client = Client.find_by(email: params[:email])
  end

    # Confirms a valid user.
    def valid_client
      unless (@client && @client.activated? &&
        @client.authenticated?(:reset, params[:id]))
      redirect_to root_url
    end
  end

  # Checks expiration of reset token.
    def check_expiration
      if @client.password_reset_expired?
        flash[:danger] = "Password reset has expired."
        redirect_to new_password_reset_url
      end
    end

end
