class MessagesController < ApplicationController
	before_action :logged_in_client, only: [:create, :destroy]
  before_action :correct_client,   only: :destroy

  def create
  	@message = current_client.messages.build(message_params)
    if @message.save
      ClientMailer.notification(@message.recipient).deliver_now
      if !current_client.admin?
        flash[:success] = "Thanks for your message! I promise to get back to you as soon as possible. You will be notified via email when I do."
      else
        flash[:success] = "Message sent to " + Client.where(email: @message.recipient).first.name + "!"
      end 
      redirect_to root_url
    else
      @feed_items = []
      render 'static_pages/home'
    end
  end

  def destroy
    if current_client.admin?
      @message.destroy
      flash[:success] = "Message Deleted!"
      redirect_to request.referrer || root_url
    else 
      redirect_to request.referrer || root_url
    end
  end

  private

  def message_params
    params.require(:message).permit(:content, :recipient)
  end

  def correct_client
    @message = current_client.messages.find_by(id: params[:id])
    redirect_to root_url if @message.nil?
  end

end
