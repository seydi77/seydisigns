
class ClientsController < ApplicationController
  before_action :logged_in_client, only: [:index, :edit, :update, :destroy]
  before_action :set_client, only: [:show, :edit, :update, :destroy]
  before_action :correct_client,   only: [:edit, :update]
  before_action :admin_client,     only: [:destroy, :index]

  # GET /clients
  # GET /clients.json
  def index
    @clientsinactive = Client.where(activated: false).paginate(page: params[:page])
    @clientsactivenonadmin = Client.where(activated: true).where(admin: false).paginate(page: params[:page])
    @clients = Client.paginate(page: params[:page])
    @clientsadmins = Client.where(admin: true).paginate(page: params[:page])
    if !current_client.admin?
      redirect_to(root_url) 
      flash[:danger] = "You need admin rights to view this page"
    end
  end

  # GET /clients/1
  # GET /clients/1.json
  def show
    @client = Client.find(params[:id])
    @messages = @client.messages.paginate(page: params[:page])
    redirect_to root_url #unless @client.activated?
  end

  # GET /clients/new
  def new
    @client = Client.new
    redirect_to(root_url) unless !logged_in? || current_client.admin?
  end

  # POST /clients
  # POST /clients.json
  def create
     @client = Client.new(client_params)
    if @client.save
      ClientMailer.account_activation(@client).deliver_now
      flash[:info] = "Please check your email to activate your account (this may take a few minutes)."
      redirect_to root_url
    else
      render 'new'
    end
  end

  # PATCH/PUT /clients/1
  # PATCH/PUT /clients/1.json

  def edit
    @client = Client.find(params[:id])
  end

  def update
    if @client.update_attributes(client_params)
      flash[:success] = "Profile updated"
      redirect_to @client
    else
      render 'edit'
    end
  end

  # DELETE /clients/1
  # DELETE /clients/1.json
  def destroy
    Client.find(params[:id]).destroy
    flash[:success] = "Client deleted"
    redirect_to clients_url
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_client
      @client = Client.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def client_params
      params.require(:client).permit(:name, :email, :password, :password_confirmation)
    end

    # Before filters

    # Confirms a logged-in client.
    def logged_in_client
      unless logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end

    # Confirms the correct client.
    def correct_client
      @client = Client.find(params[:id])
      redirect_to(root_url) unless current_client?(@client) || current_client.admin?
    end

    # Confirms an admin client.
    def admin_client
      redirect_to(root_url) unless current_client.admin?
    end

end
