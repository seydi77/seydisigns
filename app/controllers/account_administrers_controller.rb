class AccountAdministrersController < ApplicationController

	def edit

		client = Client.find_by(id: params[:id])

		if client && !client.admin? && current_client.admin?
			client.administer
			flash[:success] = client.name + " is now an Admin!"
			redirect_to clients_path
		elsif client && client.admin? && current_client.admin?
			client.deadminister
			flash[:success] = client.name + " is no longer an Admin!"
			redirect_to clients_path
		else
			flash[:danger] = "Administer didn't work for some reason, hmmmm..."
			redirect_to root_url
		end

	end
end 