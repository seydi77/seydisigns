class AccountActivationsController < ApplicationController

  def edit
    client = Client.find_by(email: params[:email])
    client2 = Client.find_by(id: params[:id])

    if client && !client.activated? && client.authenticated?(:activation, params[:id]) 
      client.activate
      log_in client
      flash[:success] = "Good news " + client.name + "! you just activated your Seydisings account! From now
      on, you can use the chat section below to message me any time you want. I promise to get back to you as soon
      as possible. Thanks!"
      redirect_to client
    elsif client2 && !client2.activated? && current_client.admin? 
      client2.activate
      flash[:success] = client2.name + " 's account is now activated!"
      redirect_to clients_path
    elsif client2 && client2.activated? && current_client.admin? 
      client2.deactivate
      if client2.admin?
        client2.deadminister
      end
      flash[:success] = client2.name + " 's account is now deactivated!"
      redirect_to clients_path
    else
      flash[:danger] = "Invalid activation link"
      redirect_to root_url
    end
  end
end