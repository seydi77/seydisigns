////////////// Splash & Cookie Stuff //////////////////////////////

// Cookie for the splash window 
function setCookie(bool, bvalue) {
  document.cookie = bool + "=" + bvalue;
}


//Returns the cookie we need 
function getCookie(bool) {
  var boolean = bool + "=";
  var ca = document.cookie.split(';');
  for(var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(boolean) == 0) {
      return c.substring(boolean.length, c.length);
    }
  }
    return ""; // return the empty string if the boolean cookie hasnt been set yet
  }

// Initialize the splashboolean to use later
var splashbool = getCookie("boolean");

// Check to see if the cookie has been set or not, set it to true uposn user visit
function checkCookie() {
  if (splashbool != "") {
    // alert("Boolean is currently false manam enmpty - Check code");
  } else {
    splashbool = "true"
    if (splashbool != "" && splashbool != null) {
      setCookie("boolean", splashbool, 365);
    }
  }
}

// A function to show the splash window
function showSplash() {
  $('.splashscreen').show();
}

function fade(){
$(".background").fadeShow({

// the aspect fill will be intact even when resizing the window

//only handy for full width / height slideshows, otherwise slows your page down
correctRatio: true,
// the slides will be shuffled before shown, get a unique slider each refresh
shuffle: true,
// milliseconds per slide
speed: 5000,
// images (urls) to create the slideshow with, array of strings
images: ['https://s3.amazonaws.com/seydisigns/2.jpg', 'https://s3.amazonaws.com/seydisigns/6.jpg','https://s3.amazonaws.com/seydisigns/4.jpg', 'https://s3.amazonaws.com/seydisigns/5.jpg']
 
});

}
/////////////////////////////////////////////////////////////////////
var ready = function(){

  //fadeshow for the jumbo
fade();
$($('.fadeShow-container')[1]).hide();

  // Load at the top of the page
  $(this).scrollTop(0);

 // Show the Splash window after 6 seconds if the user hasn't seen it yet
 if (splashbool == ""){
  setTimeout(showSplash, 4000);
 }

// If the "No thank you" splash button is clicked, the splash screen fades out
$('.nothankyou').click(function () {
  $('.splashscreen').fadeOut(500);
    checkCookie(); // update the cookie so the splash doesn't bother him again
  });
// If the "Signup" splash button is clicked, the splash screen fades out
$('.signupbutsplash').click(function () {
  $('.splashscreen').fadeOut(500);
    checkCookie(); // update the cookie so the splash doesn't bother him again
  });

// Whean loading the chat page, go to the last message in the chat
if ( $( ".homepageloggedin #chatbox" ).length ) {
  $('.homepageloggedin #chatbox').scrollTop($('#chatbox')[0].scrollHeight);
}

// Navigation stuff 
  var navoffset = $(".navbar").offset().top + 95; // value to know when to change the color of the nav

// When the user scrolls down, change the nav to clack for better clarity
$(document).scroll(function(){
  if($(this).scrollTop() > navoffset)
  {   
   $('.navbar').addClass('navhovered');
   $('.navbar-right').show();
   // $('.logo').addClass('animated fadeInDown');
   // $('.navbar-header').slideDown();
 }
});

// When the user scrolls back to the top of the page, change the color back to transparent
$(document).scroll(function(){
  if($(this).scrollTop() <= navoffset)
  {   
   $('.navbar').removeClass('navhovered');
   $('.navbar-right').hide();
   // $('.navbar-header').slideUp();
 }
});

$('.navbar ul li a').hover(function(){
$('.navbar').addClass('navhovered');
$('.navbar-right').show();
// $('.navbar-header').slideDown();
},
function(){
$('.navbar').hover(function(){
  return
},
function(){
if($(document).scrollTop() <= navoffset)
  {   
  $('.navbar').removeClass('navhovered');
  $('.navbar-right').hide();
   // $('.navbar-header').slideUp();
}
})
});

$('.dropdown-menu').hover(function(){
$('.navbar').addClass('navhovered');
$('.navbar-right').show();
},
function(){
$('.navbar').hover(function(){
  return
},
function(){
if($(document).scrollTop() <= navoffset)
  {   
  $('.navbar').removeClass('navhovered');
  $('.navbar-right').hide();
   // $('.navbar-header').slideUp();
}
})
});

$('.logoimglink').hover(function(){
$('.navbar').addClass('navhovered');
$('.navbar-right').show();
},
function(){
$('.navbar').hover(function(){
  return
},
function(){
if($(document).scrollTop() <= navoffset)
  {   
  $('.navbar').removeClass('navhovered');
  $('.navbar-right').hide();
   // $('.navbar-header').slideUp();
}
})
});

$('.dropdown').hover(function(){
$('.dropdown').addClass('open');
$('.navbar-right').show();
},
function(){
$('.dropdown').removeClass('open');
});




};
//////////////////////////// On load of the page ////////////////////////////////
// $(document).ready(ready);
$(document).on('turbolinks:load', ready);




