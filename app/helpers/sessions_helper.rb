module SessionsHelper

  # Logs in the given client.
  def log_in(client)
    session[:client_id] = client.id
  end

# Remembers a client in a persistent session.
  def remember(client)
    client.remember
    cookies.permanent.signed[:client_id] = client.id
    cookies.permanent[:remember_token] = client.remember_token
  end

# Returns true if the given user is the current user.
  def current_client?(client)
    client == current_client
  end

  # Returns the current logged-in client (if any).
  def current_client
   if (client_id = session[:client_id])
      @current_client ||= Client.find_by(id: client_id)
    elsif (client_id = cookies.signed[:client_id])
      client = Client.find_by(id: client_id)
      if client && client.authenticated?(:remember, cookies[:remember_token])
        log_in client
        @current_client = client
      end
    end
  end

  # Returns true if the client is logged in, false otherwise.
  def logged_in?
    !current_client.nil?
  end

# Forgets a persistent session.
  def forget(client)
    client.forget
    cookies.delete(:client_id)
    cookies.delete(:remember_token)
  end

  # Logs out the current client.
  def log_out
    forget(current_client)
    session.delete(:client_id)
    @current_client = nil
  end

  # Redirects to stored location (or to the default).
  def redirect_back_or(default)
    redirect_to(session[:forwarding_url] || default)
    session.delete(:forwarding_url)
  end

  # Stores the URL trying to be accessed.
  def store_location
    session[:forwarding_url] = request.original_url if request.get?
  end

end