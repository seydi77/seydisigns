# Preview all emails at http://localhost:3000/rails/mailers/user_mailer
class ClientMailerPreview < ActionMailer::Preview

  # Preview this email at
  # http://localhost:3000/rails/mailers/client_mailer/account_activation
  def account_activation
    client = Client.first
    client.activation_token = Client.new_token
    ClientMailer.account_activation(client)
  end

  # Preview this email at
  # http://localhost:3000/rails/mailers/client_mailer/password_reset
  def password_reset
    client = Client.first
    client.reset_token = Client.new_token
    ClientMailer.password_reset(client)
  end

  # Preview this email at
  # http://localhost:3000/rails/mailers/client_mailer/notification
  def notification
    client = Client.first
    ClientMailer.notification(client)
  end
end