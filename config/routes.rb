Rails.application.routes.draw do
  get 'password_resets/new'

  get 'password_resets/edit'

  get 'static_pages/home'

  # get 'static_pages/help'

  # get 'static_pages/about'

  # get 'static_pages/contact'

  get 'sessions/new'
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  resources :clients
  root 'static_pages#home'
  get  '/signup',  to: 'clients#new'
  resources :account_activations, only: [:edit]
  resources :account_administrers, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :messages,          only: [:create, :destroy]
end
