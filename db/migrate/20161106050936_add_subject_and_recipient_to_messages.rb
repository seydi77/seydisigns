class AddSubjectAndRecipientToMessages < ActiveRecord::Migration[5.0]
  def change
    add_column :messages, :subject, :string
    add_column :messages, :recipient, :string
  end
end
