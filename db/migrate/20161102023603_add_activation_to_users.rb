class AddActivationToUsers < ActiveRecord::Migration[5.0]
  def change
  	add_column :clients, :activation_digest, :string
    add_column :clients, :activated, :boolean, default: false
    add_column :clients, :activated_at, :datetime
    add_column :clients, :deactivated_at, :datetime
  end
end
