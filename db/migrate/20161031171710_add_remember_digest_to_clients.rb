class AddRememberDigestToClients < ActiveRecord::Migration[5.0]
  def change
    add_column :clients, :remember_digest, :string
  end
end
